import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.scss']
})
export class TaskListComponent implements OnInit {

  tasks = ['Andreson', 'Naguiza'];
  task = '';
  isAdmin = false;

  itens = ['Acsa', 'Adriele'];
  item = '';

  constructor() { }

  ngOnInit(): void {
  }

  add(): void {
    this.tasks.push(this.task);
  }

  addicionar(): void {
    this.itens.push(this.item);
  }

}
