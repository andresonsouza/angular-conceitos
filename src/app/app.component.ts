import { Component } from '@angular/core';
import { User } from './user';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Tasks';
  uppserText = 'display uppcarcase text';
  lowerText = 'DISPLAY LOWERCASE TEXT';
  percentvalue = 0.5;
  date: Date = new Date();
  money = 1000;
  isAdmin2 = true;
  profile = 9;

  user: User = {
    name: 'Andreson',
    age: 34
  };
}
